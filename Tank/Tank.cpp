#include "Tank.h"


Tank::Tank() : ROTATE_SPEED(5.0f), RUN_SPEED(0.3f)
{

	_move = D3DXVECTOR3(0, 0, 1);

	BOOL isHit;
	FLOAT dist;
	isHit = D3DXIntersectTri(
		&D3DXVECTOR3(0, 1, 0),
		&D3DXVECTOR3(1, 1, 0),
		&D3DXVECTOR3(1, 0, -1),
		&D3DXVECTOR3(2, 1, -0.5),
		&D3DXVECTOR3(0, -1, 0),
		NULL, NULL, &dist);

}


Tank::~Tank()
{
}

void Tank::Load()
{
	Fbx::Load("assets\\tank.fbx");
}

void Tank::Input()
{
	if (g.pInput->IsKeyPush(DIK_LEFT))
	{
		_rotate.y -= ROTATE_SPEED;
	}
	else if (g.pInput->IsKeyPush(DIK_RIGHT))
	{
		_rotate.y += ROTATE_SPEED;
	}

	if (g.pInput->IsKeyPush(DIK_UP))
	{
		_move = D3DXVECTOR3(0, 0, RUN_SPEED);
		D3DXMATRIX mat;
		D3DXMatrixRotationY(&mat, D3DXToRadian(_rotate.y));
		D3DXVec3TransformCoord(&_move, &_move, &mat);

		if (_isWallHit == FALSE)
		{
			_position += _move;
		}
		else
		{
			_position += _move - D3DXVec3Dot(&_move, &_hitWallNormal) *_hitWallNormal;
		}
	}
}

//地面と戦車の当たり判定
void Tank::CollisionGround(Fbx* ground)
{
	
	RayCastData data;
	data.start = _position;
	data.start.y += 10;

	data.dir = D3DXVECTOR3(0, -1, 0);
	ground->RayCast(&data);
	if (data.hit)
	{
		SetPosition(
			GetPosition().x,
			GetPosition().y - (data.dist - 10),
			GetPosition().z
			);
	}
}

//壁と戦車の当たり判定
void Tank::CollisionWall(Fbx* wall)
{
	RayCastData data;
	data.start = _position;
	data.dir = D3DXVECTOR3(0, 0, 1);
	D3DXMATRIX mat;
	D3DXMatrixRotationY(&mat, D3DXToRadian(_rotate.y));
	D3DXVec3TransformCoord(&data.dir, &data.dir, &mat);
	wall->RayCast(&data);
	if (data.hit && data.dist < 3.0f)
	{
		_isWallHit = TRUE;
		_hitWallNormal = data.normal;
	}
	else
	{
		_isWallHit = FALSE;
	}
}