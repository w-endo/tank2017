#pragma once
#include "MyGameEngine\Scene.h"

class Tank;
class PlayScene : public Scene
{
	Tank* _tank;
	Fbx* _ground;
	Fbx* _wall;
	Fbx* _star;

public:
	PlayScene();
	~PlayScene();

	void Init()   override;
	void Update() override;
};