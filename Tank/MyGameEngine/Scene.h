#pragma once

#include "Global.h"
#include "Sprite.h"
#include "Label.h"
#include "Quad.h"
#include "Light.h"
#include "Fbx.h"
#include <vector>

class Camera;
class Scene
{
	std::vector<Node*> _nodes;
protected:
	Camera*	_camera;
public:
	Scene();
	virtual ~Scene();
	void AddChild(Node* pNode);
	void Draw();
	virtual void Init()   = 0;
	virtual void Update();
	virtual void Input();

	void RemoveChild(Node* pNode);

	class NodeSort
	{
	public:
		bool operator()(Node* a, Node*b)
		{
			if (a->GetDistance() > b->GetDistance())
			{
				return true;
			}
			return false;
		}
	};
};