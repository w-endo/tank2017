#include "Fbx.h"


Fbx::Fbx()
{
}


Fbx::~Fbx()
{
	for (int i = 0; i < _materialCount; i++)
	{
		SAFE_RELEASE(_indexBuffer[i]);
		
	}
	delete[] _indexBuffer;
	delete[] _material;
	SAFE_DELETE(_polygonCountOfMaterial);
	_manager->Destroy();
}

Fbx* Fbx::Create(LPCSTR fileName)
{
	auto fbx = new Fbx();
	fbx->Load(fileName);
	return fbx;
}

void Fbx::Load(LPCSTR fileName)
{
	_manager = FbxManager::Create();
	_importer = FbxImporter::Create(_manager, "");
	_scene = FbxScene::Create(_manager, "");

	_importer->Initialize(fileName);
	_importer->Import(_scene);
	_importer->Destroy();

	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(fileName, NULL, 0, dir, MAX_PATH, NULL, 0, NULL, 0);
	SetCurrentDirectory(dir);

	FbxNode* rootNode = _scene->GetRootNode();
	int childCount = rootNode->GetChildCount();
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	SetCurrentDirectory(defaultCurrentDir);
}

void Fbx::Draw()
{

	g.pDevice->SetStreamSource(0, _vertexBuffer, 0, sizeof(Vertex));

	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;


	g.pDevice->SetTransform(D3DTS_WORLD, &world);
	for (int i = 0; i < _materialCount; i++)
	{
		g.pDevice->SetIndices(_indexBuffer[i]);
		g.pDevice->SetMaterial(&_material[i]);
		g.pDevice->SetTexture(0, _pTexture[i]);
		g.pDevice->DrawIndexedPrimitive(
			D3DPT_TRIANGLELIST, 0, 0, _vertexCount, 0, _polygonCountOfMaterial[i]);
	}
}



void Fbx::CheckNode(FbxNode* pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		_materialCount = pNode->GetMaterialCount();
		_material = new D3DMATERIAL9[_materialCount];
		_pTexture = new LPDIRECT3DTEXTURE9[_materialCount];
		for (int i = 0; i < _materialCount; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;
			FbxDouble3 ambient = lambert->Ambient;

			ZeroMemory(&_material[i], sizeof(_material[i]));

			_material[i].Diffuse.r = (float)diffuse[0];
			_material[i].Diffuse.g = (float)diffuse[1];
			_material[i].Diffuse.b = (float)diffuse[2];
			_material[i].Diffuse.a = 1.0f;

			_material[i].Ambient.r = (float)ambient[0];
			_material[i].Ambient.g = (float)ambient[1];
			_material[i].Ambient.b = (float)ambient[2];
			_material[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);


			if (textureFile == NULL)
			{
				_pTexture[i] = NULL;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, NULL, 0, NULL, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);

				D3DXCreateTextureFromFileEx(g.pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
					D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture[i]);
			}
		}


		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	_polygonCount = pMesh->GetPolygonCount();
	_indexCount = pMesh->GetPolygonVertexCount();
	_vertexCount = pMesh->GetControlPointsCount();



	Vertex* vertexList = new Vertex[_vertexCount];

	for (int i = 0; _vertexCount > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}


	for (int i = 0; i < _polygonCount; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);

			vertexList[index].normal =
				D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);


			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	g.pDevice->CreateVertexBuffer(sizeof(Vertex)*_vertexCount, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&_vertexBuffer, 0);

	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex)*_vertexCount);
	_vertexBuffer->Unlock();

	delete[] vertexList;



	_indexBuffer = new IDirect3DIndexBuffer9*[_materialCount];
	_polygonCountOfMaterial = new int[_materialCount];

	for (int i = 0; i < _materialCount; i++)
	{
		int* indexList = new int[_indexCount];
		int count = 0;
		for (int polygon = 0; polygon < _polygonCount; polygon++)
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		_polygonCountOfMaterial[i] = count / 3;

		g.pDevice->CreateIndexBuffer(sizeof(int)* _indexCount, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &_indexBuffer[i], 0);
		DWORD *iCopy;
		_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int)* _indexCount);
		_indexBuffer[i]->Unlock();
		delete[] indexList;
	}

}

void Fbx::RayCast(RayCastData *data)
{
	data->hit = FALSE;
	data->dist = 99999.0f;

	//頂点バッファをロック
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < _materialCount; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);



		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < _polygonCountOfMaterial[i]; j++)
		{
			//3頂点
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;



			BOOL  hit;
			float dist;

			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			if (hit && dist < data->dist)
			{
				data->hit = TRUE;
				data->dist = dist;

				D3DXVECTOR3 v1 = ver[2] - ver[0];
				D3DXVECTOR3 v2 = ver[2] - ver[1];
				D3DXVec3Cross(&data->normal, &v1, &v2);
				D3DXVec3Normalize(&data->normal, &data->normal);

			}

		}





		//インデックスバッファ使用終了
		_indexBuffer[i]->Unlock();
	}

	//頂点バッファ使用終了
	_vertexBuffer->Unlock();
}


