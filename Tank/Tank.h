#pragma once
#include "MyGameEngine\Fbx.h"
class Tank :public Fbx
{
	const float ROTATE_SPEED;
	const float RUN_SPEED;

	D3DXVECTOR3 _move;
	BOOL		_isWallHit;
	D3DXVECTOR3 _hitWallNormal;


public:
	Tank();
	~Tank();

	void Input();
	void Load();
	CREATE_FUNC(Tank);

	void SetIsWallHit(BOOL hit){_isWallHit = hit;}

	void CollisionGround(Fbx* ground);
	void CollisionWall(Fbx* wall);
};

