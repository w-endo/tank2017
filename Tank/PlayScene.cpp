#include "PlayScene.h"
#include "Tank.h"
#include "MyGameEngine\Camera.h"

PlayScene::PlayScene()
{
	_tank = nullptr;
}


PlayScene::~PlayScene()
{

}

void PlayScene::Init()
{



	auto light = Light::Create();
	this->AddChild(light);

	auto ground = Fbx::Create("Assets\\ground.fbx");
	this->AddChild(ground);
	_ground = ground;

	auto wall = Fbx::Create("Assets\\wall.fbx");
	this->AddChild(wall);
	_wall = wall;


	auto tank = Tank::Create();
	this->AddChild(tank);
	_tank = tank;


	auto star = Fbx::Create("Assets\\star.fbx");
	star->SetPosition(1, 0, 10);
	this->AddChild(star);
	_star = star;


	for (int i = 0; i < 20; i++)
	{
		auto tree = Quad::Create("Assets\\tree.png");
		tree->SetPosition(rand() % 101 - 50, 0, rand() % 101 - 50);
		tree->SetScale(2, 4, 1);
		tree->SetCamera(_camera);
		tree->SetIsSort(true);
		this->AddChild(tree);
	}


}

void PlayScene::Update()
{
	Scene::Update();

	_camera->SetTarget(_tank->GetPosition() + D3DXVECTOR3(0, 3, 0));
	D3DXVECTOR3 cam = D3DXVECTOR3(0, 5, -8);
	D3DXMATRIX mat;
	D3DXMatrixRotationY(&mat, D3DXToRadian(_tank->GetRotate().y));
	D3DXVec3TransformCoord(&cam, &cam, &mat);

	_camera->SetPosition(_tank->GetPosition() + cam);
	_camera->Update();




	_tank->CollisionGround(_ground);
	_tank->CollisionWall(_wall);


	D3DXVECTOR3 v = 
		_tank->GetPosition() - _star->GetPosition();

	float l = D3DXVec3Length(&v);
	if (l < 3.5f)
	{
		int a = 0;
	}



}

